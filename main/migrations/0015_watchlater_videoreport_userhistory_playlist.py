# Generated by Django 4.2.1 on 2023-06-02 22:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0014_channel_subscribers'),
    ]

    operations = [
        migrations.CreateModel(
            name='WatchLater',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Владелец списка')),
                ('videos', models.ManyToManyField(to='main.video', verbose_name='Видео для просмотра позже')),
            ],
            options={
                'verbose_name': "Список 'Смотреть позже'",
                'verbose_name_plural': "Списки 'Смотреть позже'",
            },
        ),
        migrations.CreateModel(
            name='VideoReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reason', models.TextField(verbose_name='Причина жалобы')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь, отправивший отчет')),
                ('video', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.video', verbose_name='Видео')),
            ],
            options={
                'verbose_name': 'Отчет о видео',
                'verbose_name_plural': 'Отчеты о видео',
            },
        ),
        migrations.CreateModel(
            name='UserHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
                ('videos', models.ManyToManyField(to='main.video', verbose_name='Просмотренные видео')),
            ],
            options={
                'verbose_name': 'История просмотра',
                'verbose_name_plural': 'Истории просмотра',
            },
        ),
        migrations.CreateModel(
            name='Playlist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Название плейлиста')),
                ('description', models.TextField(verbose_name='Описание плейлиста')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Владелец плейлиста')),
                ('videos', models.ManyToManyField(to='main.video', verbose_name='Видео в плейлисте')),
            ],
            options={
                'verbose_name': 'Плейлист',
                'verbose_name_plural': 'Плейлисты',
            },
        ),
    ]
