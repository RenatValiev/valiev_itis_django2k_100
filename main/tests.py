from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from .models import Video
from .serializers import VideoSerializer

class VideoTests(TestCase):
    def setUp(self):
        self.video1 = Video.objects.create(name='Video 1', description='Description 1')
        self.video2 = Video.objects.create(name='Video 2', description='Description 2')

    def test_get_video_list(self):
        url = reverse('video-list')
        response = self.client.get(url)
        videos = Video.objects.all()
        serializer = VideoSerializer(videos, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_create_video(self):
        url = reverse('video-list')
        data = {'name': 'New Video', 'description': 'New Description'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Video.objects.count(), 3)
        self.assertEqual(Video.objects.last().name, 'New Video')
        self.assertEqual(Video.objects.last().description, 'New Description')

    # Дополнительные тесты для других функций и моделей
